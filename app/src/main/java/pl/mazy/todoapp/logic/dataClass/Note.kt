package pl.mazy.todoapp.logic.dataClass

data class Note(
    val name: String,
    val description:String
)
